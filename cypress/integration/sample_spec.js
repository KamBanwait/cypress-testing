// group related test together with describe
// Arrange - set ini app state
// - visit a web page
// query for an element
// Act - take an action
// - interacti with that element
// Assert - make an assertion
// - make an assertion about page content

// Mocha
// Chai

// Given a user visits https://example.cypress.io
// - When they click the link labeled type
// - And they type “fake@email.com“ into the .action-email input
// - Then the URL should include /commands/actions
// - And the .action-email input has “fake@email.com“ as its value

describe('My first test', function() {
  it('Gets, types and asserts', function () {
    cy.visit('https://example.cypress.io')

    cy.contains('type').click()

    cy.url()
      .should('include', '/commands/actions')

    cy.get('.action-email')
      .type('fake@email.com')
      .should('have.value', 'fake@email.com')
  })
})
