// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
Cypress.Commands.add("login", (email, password) => {
  // visit the login page
  cy.visit('https://www.lenstore.co.uk/?_a=login')

  // get username/email field and enter username/email credential
  cy.get('input[name="username"]').type('kam@scriptedpixels.co.uk')

  // get password field and enter pass credential
  // cy.get('input[name="password"]').type('numtog-xyrdad-paFju6{enter}') // click the sign in button {enter} at the end of the email field

  // url should now be equal to .../
  // cy.hash().should('eq','https://www.lenstore.co.uk')
})
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
